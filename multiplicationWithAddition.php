<?php
/**
 * Programmiere eine Funktion zur Multiplikation von drei Faktoren: x * y * z
 * Zur Berechnung des Produkts soll jedoch KEIN Multiplikations-Operator (*) verwendet werden!
 *
 * Einschränkung: Es darf davon ausgegangen werden, dass keiner der drei Faktoren negativ ist!
 *
 * Optional: Berücksichtige in deiner Implementation auch potentiell negative Faktoren x, y, oder z.
 */

if (isset($_GET['x']) && isset($_GET['y']) && isset($_GET['z'])) {
    $x = $_GET['x'];
    $y = $_GET['y'];
    $z = $_GET['z'];
    
    if (!is_numeric($x) || !is_numeric($y) || !is_numeric($z)) {
        $validationMessage = "Es können nur Zahlen multipliziert werden!";
    }
    
    $result = multiplyplicationWithAddition($z, multiplyplicationWithAddition($x, $y));
}

function multiplyplicationWithAddition($multiplicand, $multiplier) {
    $resultIsNegative = false;
    if ($multiplicand < 0) {
        $multiplicand = -$multiplicand;
        $resultIsNegative = !$resultIsNegative;
    }
    if ($multiplier < 0) {
        $multiplier = -$multiplier;
        $resultIsNegative = !$resultIsNegative;
    }
    $result  = 0;
    for ($i = 0; $i < $multiplier; $i++) {
        $result += $multiplicand;
    }
    if ($resultIsNegative) {
        $result = -$result;
    } 
    return $result;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>PhpCheck: Multiplikation durch Addition</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>

        <div id="content">
            <h1>Multiplikation durch Addition</h1>
            <p>Es soll das Produkt der drei nachfolgenden Faktoren x, y und z ohne Verwendung des
                Multiplikations-Zeichens berechnet werden.</p>

            <form action="multiplicationWithAddition.php" method="GET" class="form center-form">
                <input type="text" name="x" title="x" value="<?php if (isset($x)): echo $x; endif; ?>"
                       placeholder="x" class="input input-monospaced input-center input-factor"/>
                
                <span class="multiplication-operator">×</span>

                <input type="text" name="y" title="y" value="<?php if (isset($y)): echo $y; endif; ?>"
                       placeholder="y" class="input input-monospaced input-center input-factor"/>

                <span class="multiplication-operator">×</span>
                
                <input type="text" name="z" title="z" value="<?php if (isset($z)): echo $z; endif; ?>"
                       placeholder="z" class="input input-monospaced input-center input-factor"/>
                <br/>
                <input type="submit" value="Resultat berechnen"/>
            </form>
            
            <?php if (isset($validationMessage)): ?>
                <p class="validation-message"><?php echo $validationMessage ?></p>
            <?php elseif (isset($result)): ?>
                <p class="result"><?php echo "{$x} × {$y} × {$z} = <b>{$result}</b>" ?></p>
            <?php endif; ?>
            
        </div>

    </body>
</html>