<?php

/**
 * Description of HeadOfSports
 *
 * @author Philipp Gabathuler
 */
class HeadOfSports extends Person {
    
    private $coach;
    
    public function startTraining() {
        echo 'Sportchef ' . $this->name . ' weist sein Trainer an die Mannschaft diese Woche jeden Tag zu Trainieren.<br>';
        for ($i = 0; $i < 7; $i++) {
            $this->coach->startTraining();
        }
    }
    
    function setCoach(Coach $coach) {
        $this->coach = $coach;
    }

}
