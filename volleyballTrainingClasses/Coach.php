<?php

/**
 * Description of Coach
 *
 * @author Philipp Gabathulers
 */
class Coach extends Person{
    
    private $players = array();      

    public function startTraining() {
        echo 'Coach ' . $this->name . ' startet das Training.<br>';
        for ($i = 0; $i < 5; $i++) {
            echo 'Coach ' . $this->name . ' weist seine Spieler an eine Runde ums Feld zu joggen.<br>';
            foreach ($this->players as $player) {
                $player->run();
            }
            echo 'Coach ' . $this->name . ' ermuntert seine Spieler 30 Liegestützen zu machen.<br>';
            foreach ($this->players as $player) {
                $player->doPushUps(30);   
            }
        }
        echo 'Coach ' . $this->name . ' beendet das Training.<br>';
    }
    
    public function addPlayer(Player $player) {
        $this->players[] = $player;
    }
    
}
