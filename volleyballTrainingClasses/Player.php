<?php


/**
 * Description of Player
 *
 * @author Philipp Gabathuler
 */
class Player extends Person{
    
    public function run() {
        echo $this->name . ' joggt eine Runde.<br>';
    }
    
    public function doPushUps(int $count) {
        echo $this->name . " macht $count Liegestützen.<br>";
    }
    
}
