# Coding Assessment
Dieses Repository enthält drei verschiedene PHP-Aufgaben. Um die Aufgaben bearbeiten und abgeben zu können, musst du ein Fork dieses Repositories erstellen. Anschliessend kannst du deinen persönlichen Fork auf deinen Computer clonen und die Aufgaben bearbeiten. Wenn du fertig bist, kannst du sämtliche Änderungen commiten und pushen. Anschliessend erstellst du (auf Bitbucket) einen Pull-Request um die Bearbeitung deiner Aufgaben abzugeben.

1. Erstelle einen (gratis) Account bei Bitbucket (falls du nicht bereits einen Account hast)
1. Logge dich mit deinem Bitbucket Account ein und surfe im Browser zu diesem Repository
1. Klicke in der linken, blauen Seitenleiste auf das _Plus-Icon_
1. Klicke auf _Fork this Repository_
1. Gib deinem persönlichen Fork einen eigenen Namen
1. Erstelle schliesslich den Fork mit einem Klick auf den Button _Fork repository_
1. Klone nun dein soeben erstelltes Repository auf deinen Computer. Wenn du nicht genau weisst wie das geht, kannst du die [Online-Dokumentation von Bitbucket](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html) konsultieren.
1. Bearbeite nun die Aufgaben... :-)
1. Wenn du fertig bist, kannst du deine Änderungen commiten und pushen (auch hier hilft dir notfalls die Online-Dokumentation von Bitbucket).
1. Logge dich nun auf Bitbucket ein, um einen Pull Request zu erstellen.
1. Gehe in dein Repository und klicke in der linken, grauen Seitenleiste auf _Pull requests_.
1. Erstelle einen neuen Pull request indem du auf den entsprechenden Link _create a pull request_ klickst.
1. Gib die erforderlichen Informationen für den Pull request ein und klicke schliesslich auf den Button _Create pull request_.
1. Fertig! Wir werden uns bei dir melden...

Falls du Fragen zu den Aufgaben und/oder zur Abgabe über Bitbucket hast, kannst du dich gerne direkt an uns wenden - am besten per E-Mail.

Wenn dir der oben beschriebene Prozess mit Bitbucket zu viel Mühe bereitet, kannst du die Quelldateien auch einfach auf deinen Computer herunterladen, bearbeiten und uns anschliessend per E-Mail senden.

**Viel Erfolg & Spass...!**