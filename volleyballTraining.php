<?php
/**
 * Der Sportchef eines Volleyballvereins ist für die sportliche Leitung seines Teams in der Nationalliga A
 * verantwortlich. Um in der kommenden Saison möglichst viele Spiele zu gewinnen, hat er einen gut ausgebildeten und
 * erfolgreichen Trainer engagiert.
 *
 * Dieser Trainer ist für die Vorbereitung und Durchführung aller Trainings des NLA-Teams verantwortlich. Da auch er
 * natürlich Erfolg haben möchte, lässt er seine Spieler hart trainieren. Dazu lässt er alle 12 Spieler regelmässig
 * joggen und 30 Liegestützen machen.
 *
 * Erstelle ein objektorientiertes Programm, welches die oben beschriebene Situation angemessen abbildet und der
 * nachfolgend eingebundenen Grafik entspricht.
 */

if (isset($_GET['startTraining'])) {

    include './volleyballTrainingClasses/Person.php';
    include './volleyballTrainingClasses/HeadOfSports.php';
    include './volleyballTrainingClasses/Coach.php';
    include './volleyballTrainingClasses/Player.php';
    
    $headsOfSports = new HeadOfSports('Meier Müller');
    $coach = new Coach('Fritz');
    $headsOfSports->setCoach($coach);
    
    $playerNames = ['Hans', 'Rudi', 'Kari', 'Michi', 'Jürg', 'Horst', 
        'Peter', 'Karl', 'Klose', 'Sämi', 'Fredi', 'Torsten'];

    foreach ($playerNames as $playerName) {
        $player = new Player($playerName);
        $coach->addPlayer($player);
    }
    
    $headsOfSports->startTraining();
    
}   

?>

<!DOCTYPE html>
<html>
    <head>
        <title>PhpCheck: Volleyball-Training</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>

        <div id="content">
            <h1>Volleyball-Training</h1>
            <p>Der Sportchef eines Volleyballvereins ist für die sportliche Leitung seines Teams in der Nationalliga A
                verantwortlich. Um in der kommenden Saison möglichst viele Spiele zu gewinnen, hat er einen gut
                ausgebildeten und erfolgreichen Trainer engagiert.</p>

            <p>Dieser Trainer ist für die Vorbereitung und Durchführung aller Trainings des NLA-Teams verantwortlich. Da
                auch er natürlich Erfolg haben möchte, lässt er seine Spieler hart trainieren. Dazu lässt er alle 12
                Spieler regelmässig joggen und 30 Liegestützen machen.</p>

            <img src="volleyball.png" alt="Abstraktion der Aufgabenstellung" title="Abstraktion der Aufgabenstellung"
                 class="volleyball-training">

            <form action="volleyballTraining.php" method="GET" class="form center-form">
                <input type="hidden" name="startTraining" value="1"/>
                <input type="submit" value="Training starten"/>
            </form>
        </div>

    </body>
</html>