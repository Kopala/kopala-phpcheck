<?php
/**
 * EURO-NOTEN
 *
 * Auf jeder Euro-Banknote ist eine eindeutige Seriennummer aufgedruckt. Diese Seriennummer besteht aus einer
 * Länderkennung (L), zehn Ziffern (Z) und einer Prüfziffer (P). Die Struktur kann folglich mit dieser Symbolfolge
 * abstrahiert werden:
 *
 *      LZZZZZZZZZZP
 *
 * Die Länderkennung ist ein einzelner Buchstabe, welcher jeweils für ein Euro-Land steht. Beispielsweise X für
 * Deutschland, N für Österreich, P für die Niederlande oder Z für Belgien.
 *
 * Der Algorithmus zur Berechnung der Prüfziffer (P) lautet folgendermassen:
 *
 *  1) Interpretiere den Ländercode (L) als Zahl entsprechend der Position im Alphabet (A=1, B=2, C=3, ...)
 *  2) Bilde die Quersumme (Q) aus den ersten 11 bzw. 12 Ziffern
 *  3) Berechne den Rest der Ganzzahldivision (R) dieser Quersumme (Q) durch 9
 *  4) Berechne die Differenz (D) von 8 und dem oben berechneten Rest der Ganzzahldivision (R): D = 8 - R
 *  5) Unterscheide die beiden Fälle a) D = 0 und b) D ≠ 0
 *      a) Falls D = 0: Prüfziffer P = 9
 *      b) Falls D ≠ 0: Prüfziffer P = 8 - R
 *
 * Erstelle ein Programm, welches eine Euro-Note anhand der Seriennummer prüft. Dazu soll die Prüfziffer berechnet und
 * mit der gegebenen Prüfziffer verglichen werden. Gib eine entsprechende Meldung auf dem Bildschirm aus, ob die
 * Banknote echt oder gefälscht ist.
 */

if (isset($_GET['serialnumber'])) {
    $serialNumber = $_GET['serialnumber'];
    
    $validationMessage;
    if(isBillSerialNumberValid($serialNumber)) {
        $validationMessage = 'Gütlige ';
    } else {
        $validationMessage = 'Ungütlige ';
    }
    $validationMessage .= ' Seriennummer für eine Euro-Banknote.';
        
}

function isBillSerialNumberValid($serialNumber) {
    if (strlen($serialNumber) != 12) {
        return false;
    }
    
    // 1) Interpretiere den Ländercode (L) als Zahl entsprechend der Position im Alphabet (A=1, B=2, C=3, ...)
    $CountryCodeChar = substr($serialNumber, 0, 1);
    $CountryCode = ord($CountryCodeChar);
    if ($CountryCode < 65 || $CountryCode > 90) {
        return false;
    }
    $CountryCode -= 64;
    
    // Überprüfen Ob Elf letzte Zeichen Zahlen sind
    $serialNumber = $CountryCode . substr($serialNumber, 1);
    if (!is_numeric($serialNumber)) {
        return false;
    }
    
    //2) Bilde die Quersumme (Q) aus den ersten 11 bzw. 12 Ziffern
    $crossSum = array_sum(str_split($serialNumber));
    
    //3) Berechne den Rest der Ganzzahldivision (R) dieser Quersumme (Q) durch 9
    $rest = $crossSum % 9;
    
    //4) Berechne die Differenz (D) von 8 und dem oben berechneten Rest der Ganzzahldivision (R): D = 8 - R
    $delta = 8 - $rest;
    
    $checkDigit = substr($serialNumber, 11);
    
    // 5) Unterscheide die beiden Fälle a) D = 0 und b) D ≠ 0 
    if ($delta == 0) {
        // a) Falls D = 0: Prüfziffer P = 9
        if ($checkDigit == 9) {
            return true;
        } 
    } else {
        // b) Falls D ≠ 0: Prüfziffer P = 8 - R
        if ($checkDigit == 8 - $rest) {
            return true;
        }
    } 
    
    return false;
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title>PhpCheck: Euro-Noten</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="style.css"/>
    </head>
    <body>

        <div id="content">
            <h1>Euro-Noten</h1>
            <p>Mit dem nachfolgenden Formular kann die Echtheit von Euro-Noten anhand der Seriennummer geprüft
                werden.</p>

            <form action="euroBills.php" method="GET" class="form center-form">
                <?php if (isset($serialNumber)): ?>
                    <input type="text" name="serialnumber" title="serialnumber"
                           value="<?php echo $serialNumber ?>"
                           class="input input-monospaced input-center"/>
                <?php else: ?>
                    <input type="text" name="serialnumber" title="serialnumber"
                           class="input input-monospaced input-center"/>
                <?php endif; ?>
                <br/>
                <input type="submit" value="Seriennummer prüfen"/>
            </form>
            
            <?php if (isset($validationMessage)): ?>
                <p class="validation-message"><?php echo $validationMessage ?></p>
            <?php endif; ?>
        </div>

    </body>
</html>