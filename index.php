<?php


?>

<!DOCTYPE html>
<html>
    <head>
        <title>Coding-Assesment</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
        <link rel="stylesheet" href="style.css"/>
    </head>

    <body>
        <div id="content">
            <h1>Coding-Assessment</h1>
            <p>Die nachfolgenden drei Aufgaben sollen es uns ermöglichen, deine Fähigkeiten als PHP-Programmierer
                einzuschätzen.</p>
            <p>Löse dazu diese Aufgaben bitte innerhalb der nächsten Tage. Zur Bearbeitung der Aufgaben darst du -
                sofern
                erforderlich - selbstverständlich im Internet recherhieren oder andere Hilfsmittel beiziehen. Du
                solltest
                die Aufgaben jedoch ohne persönliche Unterstützung durch eine andere Person lösen.</p>

            <ol class="task-list">
                <li>
                    <span class="task-title"><a href="euroBills.php">Euro-Noten</a></span>
                    <span class="task-description">Implementiere einen Algorithmus, welcher gefälsche Euro-Banknoten
                        anhand der Seriennummer erkennen kann.</span>
                </li>

                <li>
                    <span class="task-title"><a href="multiplicationWithAddition.php">Multiplikation durch Addition</a></span>
                    <span class="task-description">Aus drei Faktoren soll ohne Verwendung des Multiplikations-Zeichens
                        das Produkt berechnet werden.</span>
                </li>

                <li>
                    <span class="task-title"><a href="volleyballTraining.php">Volleyball-Training</a></span>
                    <span class="task-description">Eine beschriebene und schematisch dargestellte Situation soll
                        objektorientiert umgesetzt werden.</span>
                </li>
            </ol>

            <p>Falls du Fragen zu den Aufgaben oder zur Umsetzung hast, darst du dich natürlich jederzeit an uns wenden.
                Schreibe am besten ein <a href="mailto:pg@clicsoft.ch">E-Mail an Peter</a>!</p>
        </div>
    </body>
</html>